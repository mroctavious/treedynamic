#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <string.h>


//Enum para los archivos de salida y entrada de cada tipo
typedef enum Salida
{
	RDFNB=1,
	RDF=2,
	FINALCONF=3,
	MATHEMATICA=4,
	ENERGY=5,
	NMC=6,
	INITCONF=7,
	INITNB=8,
	BOX=9,
	METROPOLIDIAM=10
} Salida;

//Errores posibles durante la ejecucion del programa
typedef enum Error
{
	SYNTAX_ERROR=0,
	FILE_OPEN_ERROR=-1,
	FILE_CREATE_ERROR=-2
} Error;


int help( Error errorNum, char *msg );


int getError( Error errorNum );


char *filenameGenerator( char *output, const char *ext );


//Funcion que genera strings
char *fileType( char *output, Salida type );

int readNmc(char *input, int *nmcold, int *ngrold );



int readRDF(char *input, unsigned long *g, int ndivr );


int readConf(char *input, float *x, float *y, int n );




//Funcion que lee archivo de texto y carga configuraciones
int readInput(	char *fileName, int *nmc, int *nsub, int *n, float *rho, float *sigma, float *Ra, float *Rr, float *dpmax, char *newprog, char *orden);


//Funcion que calculara los nuevos sigmas
//a partir del archivo de configuracion
void getSigmas(float *sigma2, float *sigma_m, float sigma);

void areas( float *area, int n, float rho, float *lx, float *ly, float *lx2, float *ly2, float *deltar, int *ndivr);

double traslation( double old , float dpmax);

//Crear dimenciones de la caja y guardar en archivo
int writeBox( char *outputName, float lx2, float ly2 );

int calculateMetropolis(double de);

//Conseguir distancia minima
double mindist(double x1, double y1, double x2, double y2, float lx, float ly, float lx2, float ly2);

//Evitar traslape entre particulas
int baby(double xt, double yt, int i, float *dr, float *x, float *y, float lx, float ly, float lx2, float ly2, float sigma2);

double energiaParticula( float *x, float *y, double xt, double yt, int i, int n, float lx, float ly, float lx2, float ly2, float Ra, float Rr, float Ra2, float Rr2, double epsilon_a, double epsilon_r, float sigma2);

//Redefine las coordenadas de la particula de acuerdo a las condiciones periodicas
void condicionesPeriodicas(double *xx, double *yy, float lx, float lx2, float ly, float ly2);

//Unidades Reducidas
void reducedUnits(float sigma, float *sigma2, float *sigma_m, float *Ra, float *Rr, float *Ra2, float *Rr2, float *rc, double *epsilon_a, double *epsilon_r );


//Funcion Traslape
int traslape(double xt, double yt,int i, int n, float *x, float *y, float *dr, float lx, float lx2, float ly, float ly2, float sigma2);

///Funcion montecarlo, al parecer esto se va a paralelizar
int mc( char *output, float *x, float *y, unsigned long *g, int nmc, int n, float dpmax, float lx, float lx2, float ly, float ly2, float sigma2, float Ra, float Rr, float Ra2, float Rr2, double epsilon_a, double epsilon_r, float deltar, int nmctot, int ngrold);;


int initialConfig(char *output, char orden, float *x, float *y, float lx, float ly, float lx2, float ly2, int n, float *dr, float sigma, float sigma2, float sigma_m );


#include "tDynamic.c"
