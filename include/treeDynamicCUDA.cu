
__device__ int cudaMetropolis( float de, float b )
{
	int accept=0;
	float fb;
	float temp;

	temp=0.143;
	if( de < 0 )
		accept=1;
	else
	{
		//b=(float) rand()/RAND_MAX;
		fb=exp(-de/temp);
		if( fb > b )
			accept=1;
		else
			accept=0;
	}

	return accept;
}


//Funcion que hara la operacion atomica con doubles
__device__ double atomicAddD(double* address, double val)
{

	unsigned long long int* address_as_ull =(unsigned long long int*)address;
	unsigned long long int old = *address_as_ull, assumed;
	do
	{
		assumed = old;
		old = atomicCAS(address_as_ull, assumed, __double_as_longlong(val + __longlong_as_double(assumed)));
	}
	while (assumed != old);
	return __longlong_as_double(old);
}


__device__ void lastPartMC(int *mov, float *x, float *y, int i, float *Enew, float *Eold, float xnew, float ynew, float xold, float yold, int *accept, float random )
{
	//Enew = energiaParticula(x, y, xnew,ynew,i, n, lx, ly, lx2, ly2, Ra, Rr, Ra2, Rr2, epsilon_a, epsilon_r,sigma2);
	float dE = *Enew - *Eold;

	*mov = cudaMetropolis(dE,random);

	if (*mov==1)
	{
		//printf("hubo aceptacion %d \n", mov);
		x[i] = xnew;
		y[i] = ynew;
		Eold = Enew;
		accept++;
	}
	else
	{
		x[i] = xold;
		y[i] = yold;
	}

}

//Funcion distancia minima
__device__ double devMindist(double x1, double y1, double x2, double y2, float lx, float ly, float lx2, float ly2)
{
	//cuPrintf("devMindist Reciviendo: x1=%f, y1=%f, x2=%f, y2=%f, lx=%f, ly=%f, lx2=%f, ly2=%f\n", x1, y1, x2, y2, lx, ly, lx2, ly2);
	float dx=fabs(x1 - x2);
	if(dx > lx2)
	{
		dx = dx - lx;
	}

	float dy = fabs(y1 - y2);
	if (dy > ly2)
	{
		dy=dy-ly;
	}
	//cuPrintf("Devolviendo %f\n", dx*dx+dy*dy);
	return dx*dx+dy*dy;
}


__global__ void energiaParticulaCUDA( float *pot, float *x, float *y, double xt, double yt, int i, int n, float lx, float ly, float lx2, float ly2, float Ra, float Rr, float Ra2, float Rr2, double epsilon_a, double epsilon_r, float sigma2)
{
	//Conseguir el hilo
	int blockX=blockIdx.x;
	int blockY=blockIdx.y;
	//int hiloX=threadIdx.x;

	int index=blockDim.y*blockX+blockY;

	//int index=blockY;
	//printf("Soy el hilo : %d\n", index);
	if( index < n )
	{
		double ulr=0;
		double patr, prep; //patr: Potencial de atracción; prep: Potencial de repulsión
		double expa, expr; //expa: exponencial atracción; expr: exponencial repulsión
		double rr;
		if( index != i )
		{
			rr=sqrt(devMindist(xt, yt, x[index], y[index], lx, ly, lx2, ly2));
			expa=exp(-(rr/Ra));
			expr=exp(-(rr/Rr));
			//cuPrintf("expa=%f   expr=%f\n", expa, expr);
			//cuPrintf("Ra=%f   Rr=%f\n", Ra, Rr);
			//cuPrintf("Exp(%f)\n", -(rr/Rr) );
			patr = (-(epsilon_a*sigma2)/Ra2)*expa;
			prep = ((epsilon_r*sigma2)/Rr2)*expr;

			ulr = patr + prep;

			if( ulr != 0.0 )
			{
				//cuPrintf("Operacion atomica! Valor anterior : %f \n",atomicAddD(pot, ulr) );
				atomicAdd(pot, ulr);
			}
		}

	}
}



__global__ void energiaParticulaCUDALastPart( float *pot, float *x, float *y, double xt, double yt, int i, int n, float lx, float ly, float lx2, float ly2, float Ra, float Rr, float Ra2, float Rr2, double epsilon_a, double epsilon_r, float sigma2, int *mov, float *Eold, float xold, float yold, int *accept, float random)
{
	//Conseguir el hilo
	int blockX=blockIdx.x;
	int blockY=blockIdx.y;
	//int hiloX=threadIdx.x;

	int index=blockDim.y*blockX+blockY;
	if( *mov == 1 )
	{
		//int index=blockY;
		//printf("Soy el hilo : %d\n", index);
		if( index < n )
		{
			double ulr=0;
			double patr, prep; //patr: Potencial de atracción; prep: Potencial de repulsión
			double expa, expr; //expa: exponencial atracción; expr: exponencial repulsión
			double rr;
			if( index != i )
			{
				rr=sqrt(devMindist(xt, yt, x[index], y[index], lx, ly, lx2, ly2));
				expa=exp(-(rr/Ra));
				expr=exp(-(rr/Rr));
				//cuPrintf("expa=%f   expr=%f\n", expa, expr);
				//cuPrintf("Ra=%f   Rr=%f\n", Ra, Rr);
				//cuPrintf("Exp(%f)\n", -(rr/Rr) );
				patr = (-(epsilon_a*sigma2)/Ra2)*expa;
				prep = ((epsilon_r*sigma2)/Rr2)*expr;

				ulr = patr + prep;

				if( ulr != 0.0 )
				{
					//cuPrintf("Operacion atomica! Valor anterior : %f \n",atomicAddD(pot, ulr) );
					atomicAdd(pot, ulr);
				}
			}

		}


		__syncthreads();
		if( index == 1 )
		{
			{
				lastPartMC(mov, x, y, i, pot, Eold, xt, yt, xold, yold, accept, random );
			}
		}
	}
}


__device__ float conditionsGPU(double xx, double yy, float lx, float lx2, float ly, float ly2)
{
	int signox;

	if (xx > lx2) signox = -1;
	if (xx < lx2) signox =  1;
	if((xx > -lx2) && (xx < lx2)) signox = 0;
	return xx + signox*lx;
}


__global__ void cudaResetFlag(int *flag, int value)
{
	*flag=value;
}

//Funcion Traslape
__global__ void traslapeParallel(int *flag, double xt, double yt,int i, int n, float *x, float *y, float lx, float lx2, float ly, float ly2, float sigma2)
{
	//Get index number
	int index=getIndex1D1D();

	//Si no es la misma particula
	if ( index != i )
	{
		double dr=devMindist(xt, yt, x[index], y[index], lx, ly, lx2, ly2);
		if ( dr <= sigma2)
		{
			*flag=0;
		}
	}
}


__global__ void hello(const char * str)
{
	printf("%s\n", str);
}
