#include "cuPrintf.cuh"
#include "cuPrintf.cu"
#include "cudaIndex.cu"


__device__ int cudaMetropolis( float de, float b );


//Funcion que hara la operacion atomica con doubles
__device__ double atomicAddD(double* address, double val);


__device__ void lastPartMC(int *mov, float *x, float *y, int i, float *Enew, float *Eold, float xnew, float ynew, float xold, float yold, int *accept, float random );

//Funcion distancia minima
__device__ double devMindist(double x1, double y1, double x2, double y2, float lx, float ly, float lx2, float ly2);


__global__ void energiaParticulaCUDA( float *pot, float *x, float *y, double xt, double yt, int i, int n, float lx, float ly, float lx2, float ly2, float Ra, float Rr, float Ra2, float Rr2, double epsilon_a, double epsilon_r, float sigma2);


__global__ void energiaParticulaCUDALastPart( float *pot, float *x, float *y, double xt, double yt, int i, int n, float lx, float ly, float lx2, float ly2, float Ra, float Rr, float Ra2, float Rr2, double epsilon_a, double epsilon_r, float sigma2, int *mov, float *Eold, float xold, float yold, int *accept, float random);


__device__ float conditionsGPU(double xx, double yy, float lx, float lx2, float ly, float ly2);


__global__ void cudaResetFlag(int *flag, int value);

//Funcion Traslape
__global__ void traslapeParallel(int *flag, double xt, double yt,int i, int n, float *x, float *y, float lx, float lx2, float ly, float ly2, float sigma2);

__global__ void hello(const char * str);

#include "treeDynamicCUDA.cu"
