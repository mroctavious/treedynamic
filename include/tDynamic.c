void mathematica( char *input, float *x, float *y, float sigma_m, int n)
{

	int i;
	FILE *fm;
	fm = fopen(input, "w");

	fprintf(fm,"Graphics[{");
	fprintf(fm,"{Red, Disk[{%f, %f}, %f]},",x[0],y[0],sigma_m);

	for (i=1; i<n; i++)
		fprintf(fm,",{Red, Disk[{%f, %f}, %f]}",x[i],y[i],sigma_m);

	fprintf(fm,"}]");
	fclose(fm);
}

int help( Error errorNum, char *msg )
{
	if( errorNum == SYNTAX_ERROR )
	{
		printf("Error de syntaxis!\nIntente ejecutar como el siguiente ejemplo\n\t\t./treeDynamics inputFolder outputFolde\n");
	}
	else if( errorNum == FILE_OPEN_ERROR )
	{
		printf("Error al intentar leer archivo!\nAsegurate que existe el archivo %s\n", msg);

	}
	else if( errorNum == FILE_CREATE_ERROR )
	{
		printf("Error al intentar crear archivo!\nAsegurate que tienes suficiente espacio y privilegio para escribir en la carpeta %s\n", msg);
	}
	else
	{
		//CUSTOM ERROR
		printf("%s\n", msg);
	}

	return errorNum;
}


int getError( Error errorNum )
{
	int error=0;

	if( errorNum == SYNTAX_ERROR )
		error=1;

	else if( errorNum == FILE_OPEN_ERROR )
		error=-1;

	else if( errorNum == FILE_CREATE_ERROR )
		error=-2;

	return error;
}


char *filenameGenerator( char *output, const char *ext )
{
	//Creacion de strings para archivos de salida
	//String para archivo de caja
	char *fileName=(char *) malloc(sizeof(char) * strlen(output) + strlen(ext) + 1 ); //Length of output name + ".box\0"=5

	//Copiar Contenido del string output y guardarlo en el string que acabamos de reservar 'fileName'
	strcpy(fileName, output);

	//Concatenar la extencion
	strcat(fileName, ext);

	//Regresar string
	return fileName;
}


//Funcion que genera strings
char *fileType( char *output, Salida type )
{

	if( type == BOX )
	{
		return filenameGenerator( output, ".box" );
	}


	else if( type == METROPOLIDIAM )
	{
		return filenameGenerator( output, ".mdia" );
	}


	else if( type ==  RDFNB )
	{
		return filenameGenerator( output, ".rdf.nb" );
	}


	else if( type == RDF )
	{
		return filenameGenerator( output, ".rdf" );
	}


	else if( type == FINALCONF )
	{
		return filenameGenerator( output, ".finalConf" );
	}


	else if( type == MATHEMATICA )
	{
		return filenameGenerator( output, ".final.nb" );
	}


	else if( type == ENERGY )
	{
		return filenameGenerator( output, ".energy" );
	}


	else if( type == NMC )
	{
		return filenameGenerator( output, ".nmc" );
	}


	else if( type ==  INITCONF)
	{
		return filenameGenerator( output, ".initConf" );
	}


	else if( type == INITNB )
	{
		return filenameGenerator( output, ".init.nb" );
	}


	else
	{
		return NULL;
	}


}

int readNmc(char *input, int *nmcold, int *ngrold )
{

	FILE *fnmc=NULL;

	//Conseguimos el nombre del archivo de entrada
	char *inputFile=fileType( input, NMC );

	//Intentamos abrir el archivo
	fnmc = fopen( inputFile, "r");

	//Verificar que se haya podido abrir
	if( fnmc == NULL )
	{
		help(FILE_OPEN_ERROR, inputFile);
		free(inputFile);

		return getError(FILE_OPEN_ERROR);
	}

	//Leer desde archivo
	fscanf(fnmc,"%d %d", nmcold, ngrold);

	//Liberamos memoria
	fclose(fnmc);
	free(inputFile);

	//0 Errores
	return 0;
}



int readRDF(char *input, unsigned long *g, int ndivr )
{

	FILE *frdf=NULL;
	int j;
	float trash;

	//Conseguimos el nombre del archivo de entrada
	char *inputFile=fileType( input, RDF );

	//Intentamos abrir el archivo
	frdf = fopen( inputFile, "r");

	//Verificar que se haya podido abrir
	if( frdf == NULL )
	{
		help(FILE_OPEN_ERROR, inputFile);
		free(inputFile);
		return getError(FILE_OPEN_ERROR);

	}
	for( j=0; j<ndivr; j++ )
	{
		//Leer desde archivo
		fscanf( frdf,"%f %lu %f", &trash, &g[j], &trash );
	}

	//Liberamos memoria
	fclose(frdf);
	free(inputFile);

	//0 Errores
	return 0;
}


int readConf(char *input, float *x, float *y, int n )
{

	FILE *file=NULL;
	int j;

	//Conseguimos el nombre del archivo de entrada
	char *inputFile=fileType( input, FINALCONF );

	//Intentamos abrir el archivo
	file = fopen( inputFile, "r");

	//Verificar que se haya podido abrir
	if( file == NULL )
	{
		help(FILE_OPEN_ERROR, inputFile);
		free(inputFile);
		return getError(FILE_OPEN_ERROR);
	}

	//Leer desde archivo
	for( j=0; j<=n; j++ )
	{
		fscanf( file,"%f %f", &x[j], &y[j] );
	}

	//Liberamos memoria
	fclose(file);
	free(inputFile);

	//0 Errores
	return 0;
}




//Funcion que lee archivo de texto y carga configuraciones
int readInput(	char *fileName, int *nmc, int *nsub, int *n, float *rho, float *sigma, float *Ra, float *Rr, float *dpmax, char *newprog, char *orden)
{
	FILE *file;
	file = fopen( fileName, "r" );

	//If couldnt open file, return error
	if( file == NULL )
		return -1;
	fscanf( file, "%d %d", nmc, nsub );
	fscanf( file, "%d %f", n, rho );
	fscanf( file, "%f", sigma );
	fscanf( file, "%f %f", Ra, Rr );
	fscanf( file, "%f ", dpmax );
	fscanf(file, "%c ", newprog);
	fscanf(file, "%c", orden );
	fclose(file);

	return 0;
}


//Funcion que calculara los nuevos sigmas
//a partir del archivo de configuracion
void getSigmas(float *sigma2, float *sigma_m, float sigma)
{
	*sigma2=sigma*sigma;
	*sigma_m=sigma/2.0;
	return;
}

void areas( float *area, int n, float rho, float *lx, float *ly, float *lx2, float *ly2, float *deltar, int *ndivr)
{
	*area=(float) n/rho;
	*lx=sqrt(*area);
	*ly=*lx;
	*lx2= *lx / 2.0;
	*ly2= *ly / 2.0;

	*deltar=0.05;

	*ndivr=floor( *lx2 / *deltar );

	return;
}

double traslation( double old , float dpmax)
{
	return old + (2.0*(double)rand()/(double)RAND_MAX-1.0)*dpmax;
}

//Crear dimenciones de la caja y guardar en archivo
int writeBox( char *outputName, float lx2, float ly2 )
{
	FILE *Box;
	Box = fopen(outputName,"w");
	if( Box == NULL )
		return -2;

	//Se imprime dimenciones de la caja
	fprintf(Box,"%f %f \n",-lx2,-ly2);
	fprintf(Box,"%f %f \n",-lx2,ly2);
	fprintf(Box,"%f %f \n",lx2,ly2);
	fprintf(Box,"%f %f \n",lx2,-ly2);
	fprintf(Box,"%f %f \n",-lx2,-ly2);
	fclose(Box);

	//No hubo error
	return 0;
}

int calculateMetropolis(double de)
{
	int accept=0;
	double fb;
	double b;
	double temp;

	temp=0.143;
	if( de < 0 )
		accept=1;
	else
	{
		b=(double) rand()/RAND_MAX;
		fb=exp(-de/temp);
		if( fb > b )
			accept=1;
		else
			accept=0;
	}

	return accept;
}

//Conseguir distancia minima
double mindist(double x1, double y1, double x2, double y2, float lx, float ly, float lx2, float ly2)
{
	float dx=fabs(x1 - x2);
	if(dx > lx2)
	{
		dx = dx - lx;
	}

	float dy = fabs(y1 - y2);
	if (dy > ly2)
	{
		dy=dy-ly;
	}
	return dx*dx+dy*dy;
}


int baby(double xt, double yt, int i, float *dr, float *x, float *y, float lx, float ly, float lx2, float ly2, float sigma2)
{
	int j;
	int u=1;
	if(i > 1)
	{
		for(j=1;j<=i-1;j++)
		{
			*dr=mindist( xt, yt, x[j], y[j], lx, ly, lx2, ly2 );
			if(*dr <= sigma2)
			{
				u=0;
				break;
			}
			else
				u=1;
		}
	}
	return u;
}




double energiaParticula( float *x, float *y, double xt, double yt, int i, int n, float lx, float ly, float lx2, float ly2, float Ra, float Rr, float Ra2, float Rr2, double epsilon_a, double epsilon_r, float sigma2)
{
	double patr, prep; //patr: Potencial de atracción; prep: Potencial de repulsión
	double expa, expr; //expa: exponencial atracción; expr: exponencial repulsión
	double rr;
	double pot=0, ulr=0;

	int j;
	for( j=1; j <= n; j++)
	{
		ulr=0;
		if( j != i )
		{
			rr=sqrt(mindist(xt, yt, x[j], y[j], lx, ly, lx2, ly2));
			expa=exp(-(rr/Ra));
			expr = exp(-(rr/Rr));
			//printf("Exp( %f )= %f\n", -(rr/Ra), expa);
			patr = (-(epsilon_a*sigma2)/Ra2)*expa;
			prep = ((epsilon_r*sigma2)/Rr2)*expr;
			ulr = patr + prep;
		}
		pot+=ulr;
	}
	return pot;
}

void condicionesPeriodicas(double *xx, double *yy, float lx, float lx2, float ly, float ly2)
{
	int signox;
	int signoy;

	if (*xx > lx2) signox = -1;
	if (*xx < lx2) signox =  1;
	if ((*xx > -lx2) && (*xx < lx2)) signox = 0;
	*xx = *xx + signox*lx;
	if (*yy > ly2) signoy = -1;
	if (*yy < ly2) signoy =  1;
	if ((*yy > -ly2) && (*yy < ly2)) signoy = 0;
	*yy = *yy + signoy*ly;
}

//Unidades Reducidas
void reducedUnits(float sigma, float *sigma2, float *sigma_m, float *Ra, float *Rr, float *Ra2, float *Rr2, float *rc, double *epsilon_a, double *epsilon_r )
{
	*sigma2=sigma*sigma;
	*sigma_m=sigma/2.0;
	*Ra = (*Ra) * sigma;
	*Rr = (*Rr) * sigma;
	*Ra2 = (*Ra) * (*Ra) ;
	*Rr2 = (*Rr) * (*Rr);
	*rc = 2.5*sigma;
	*epsilon_a = 1.0;
	*epsilon_r = 1.0;
}


//Funcion Traslape
int traslape(double xt, double yt,int i, int n, float *x, float *y, float *dr, float lx, float lx2, float ly, float ly2, float sigma2)
{
	int j,u;
	u=1;
	for (j = 1; j <= n;j++)
	{
		if (j != i)
		{
			*dr=mindist(xt, yt, x[j], y[j], lx, ly, lx2, ly2);

			if (*dr<=sigma2)
			{
				u=0;
				break;
			}
			else
				u=1;
		}
	}
	return u;
}


///Funcion montecarlo, al parecer esto se va a paralelizar
/*int mc( char *output, float *x, float *y, unsigned long *g, int nmc, int n, float dpmax, float lx, float lx2, float ly, float ly2, float sigma2, float Ra, float Rr, float Ra2, float Rr2, double epsilon_a, double epsilon_r, float deltar, int nmctot, int ngrold)
{
	int i,j,ir,in,mov;
	//int nsubmc=0;
	int ngr=0;
	int accept=0;
	float dr;
	//Energia
	double Eold, Enew, dE;
	double xold,yold;
	double xnew,ynew;

	//Nombre de los archivos
	char *outFileFConf=fileType( output, FINALCONF );
	char *outFileNMC=fileType( output, NMC );

	//Crear archivos de salida
	FILE *ff;
	FILE *fnmc;
	ff = fopen( outFileFConf, "w" );
	fnmc = fopen( outFileNMC, "w" );

	//Verificar que se haya podido crear archivo
	if( ff == NULL )
	{
		help( FILE_CREATE_ERROR, outFileFConf );
		free(outFileFConf);
		free(outFileNMC);
		return FILE_CREATE_ERROR;
	}

	//Verificar que se haya podido crear archivo NMC
	if( fnmc == NULL )
	{
		help( FILE_CREATE_ERROR, outFileNMC );
		free(outFileFConf);
		free(outFileNMC);
		return FILE_CREATE_ERROR;
	}


	int imc;
	for (imc=1;imc<=nmc;imc++)
	{
		for (in=1;in<=n-1;in++)
		{
			i = rand()%n +1;
			//printf("Intento de mover particula %d\n",i);
			//printf("\n");
			
			//CUDA MOV SET 0
			mov=0;
			


			//printf("mov %d \n",mov);
			xold=x[i];
			yold=y[i];
			//printf("coordenadas viejas %f %f\n\n", xold,yold);
			//Eold = energiaParticula(x, y, xold, yold, i, n, lx, ly, lx2, ly2, Ra, Rr, Ra2, Rr2, epsilon_a, epsilon_r,sigma2 );
			//printf("Energia CPU:%f\n", Eold);
			Eold = paralleEnergy(x, y, xold, yold, i, n, lx, ly, lx2, ly2, Ra, Rr, Ra2, Rr2, epsilon_a, epsilon_r,sigma2 );
			//printf("Energia GPU:%f\n", Eold);
			xnew=traslation(xold, dpmax);
			ynew=traslation(yold, dpmax);
			if ((ynew > -ly2) && (ynew < ly2))
			{
				xnew=condicionesPeriodicas(xnew, ynew, lx, lx2, ly, ly2); // mejorar
				mov=traslape(xnew, ynew, i, n, x, y, &dr, lx, lx2, ly, ly2, sigma2);

				if ( mov == 1)
				{
					Enew=paralleEnergy(x, y, xnew, ynew, i, n, lx, ly, lx2, ly2, Ra, Rr, Ra2, Rr2, epsilon_a, epsilon_r,sigma2 ); //Parallel
					//Enew = energiaParticula(x, y, xnew,ynew,i, n, lx, ly, lx2, ly2, Ra, Rr, Ra2, Rr2, epsilon_a, epsilon_r,sigma2);
					dE = Enew - Eold;
					mov = calculateMetropolis(dE);

					if (mov==1)
					{
						x[i] = xnew;
						y[i] = ynew;
						Eold = Enew;
						accept++;
					}
					else
					{
						x[i] = xold;
						y[i] = yold;
					}
				}
			}
		}//Fin 2do FOR
		printf("IMC=%d\n", imc);
		//* calculo de la funcion de distribucion radial */
/*		for (j=1;j<=n-1;j++)
		{
			if (j!=i)
			{
				dr=mindist(x[j],y[j],x[i],y[i], lx, ly, lx2, ly2 );
				dr=sqrt(dr);
				if (dr<=lx2)
				{
					ir=floor(dr/deltar);
					g[ir]=g[ir]+1;
				}
			}
		}
		ngr++;
	}

/*	if ((imc % nsub) == 0)
	{
		nsubmc++;
		printf("nmc %d, nsubrutina %d, numero de rdf %d \n",imc,nsubmc,ngr);
		printf("aceptacion %f\n",(double)acept/(imc*n));
		energiaw(imc,Eold);
	}
*/
/*	for (j=1;j<=n;j++)
	{
		fprintf(ff,"%f\t%f\n",x[j],y[j]);
	}
	int ngrtot=ngr+ngrold;
	fprintf(fnmc,"%d %d\n",nmctot,ngrtot);

	free(outFileFConf);
	free(outFileNMC);

        fclose(ff);
        fclose(fnmc);
	printf("Termina subrutina Monte Carlo \n");
	return 0;
}

*/


int initialConfig(char *output, char orden, float *x, float *y, float lx, float ly, float lx2, float ly2, int n, float *dr, float sigma, float sigma2, float sigma_m )
{
	printf("Inicia la configuracion de las particulas\n");
	int i,j,k,born;
	int ncx, ncy;
	float ax, ay;
	double xt,yt;
	double xo1,yo1,xo2,yo2;

	//Conseguimos el nombre del archivo de entrada
	char *outFileConf=fileType( output, INITCONF );
	char *outFileNb=fileType( output, INITNB );

	FILE *fp;
	fp = fopen( outFileConf, "w");

	FILE *fmi;
	fmi = fopen( outFileNb, "w");
	fprintf(fmi,"Graphics[{");

	if( fp == NULL || fmi == NULL )
	{
		help( FILE_CREATE_ERROR, outFileConf);
		free(outFileConf);
		free(outFileNb);
		return FILE_CREATE_ERROR;
	}

	i=1;
	born=1;

	switch(orden)
	{
		case 'd':
			printf("Se utiliza una configuracion inicial desordenada\n");

			//Estaba asi: while (i<=n), le agregue el 1, porque se sale de los indices
			while (i<=n-1)
			{
				xt = (2.0*(double)rand()/(double)RAND_MAX-1.0)*lx2;
				yt = (2.0*(double)rand()/(double)RAND_MAX-1.0)*ly2;
				born=baby( xt, yt, i, dr, x, y, lx, ly, lx2, ly2, sigma2);
				if (born==1)
				{
					x[i]=xt;
					y[i]=yt;
					fprintf(fp,"%f\t%f\t%d\n",x[i],y[i],i);
					fprintf(fmi,"{Red, Disk[{%f, %f}, %f]},",x[i],y[i],sigma_m);
					//printf("nace particula %d\n",i);
					i++;
				}
	 		}
			break;
		case 'o':
			printf("Se utiliza una configuracion inicial ordenada\n");
			ncx=sqrt((double)n/2.0);
			ncy=ncx;
			ax=lx/ncx;
			ay=ly/ncy;
			xo1=-lx2+ax/4.0;
			yo1=-ly2+ay/4.0;
			xo2=-lx2+3.0*ax/4.0;
			yo2=-ly2+3.0*ax/4.0;
			printf("origen de x %f %f\n",xo1,xo2);
			printf("origen de y %f %f\n",yo1,yo2);

			while (i<=n)
			{
				for (k=1;k<=(int)ncx;k++)
				{
					for (j=1;j<=(int)ncy;j++)
					{
						x[i]=xo1 + (j-1)*ax;
						y[i]=yo1 + (k-1)*ay;
						//printf("nace particula %d\n",i);
						fprintf(fp,"%f\t%f\t%d\n",x[i],y[i],i);
						fprintf(fmi,"{Red, Disk[{%f, %f}, %f]},",x[i],y[i],sigma_m);
						i++;
						x[i]=xo2 + (j-1)*ax;
						y[i]=yo2 + (k-1)*ay;
						//printf("nace particula %d\n",i);
						fprintf(fp,"%f\t%f\t%d\n",x[i],y[i],i);
						fprintf(fmi,"{Red, Disk[{%f, %f}, %f]},",x[i],y[i],sigma/2.0);
						i++;
					}
				}
			}
			break;
		default:
			break;
	}

	fprintf(fmi,"Black,Line[{{-%f,%f},{%f,%f},{%f,-%f},{-%f,-%f},{-%f,%f}}]}]",lx2,ly2,lx2,ly2,lx2,ly2,lx2,ly2,lx2,ly2);
	free(outFileConf);
	free(outFileNb);

	fclose(fp);
	fclose(fmi);
	printf("Termina la configuracion de las particulas\n");
	return 0;
}

