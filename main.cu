#include "include/tDynamic.h"
#include "include/treeDynamicCUDA.cuh"
#define BlockX 3
#define THREADS 800




///Funcion montecarlo, al parecer esto se va a paralelizar
int almostParallelMC( char *output, float *x, float *y, unsigned long *g, int nmc, int n, float dpmax, float lx, float lx2, float ly, float ly2, float sigma2, float Ra, float Rr, float Ra2, float Rr2, double epsilon_a, double epsilon_r, float deltar, int nmctot, int ngrold, int *particulas, float *aleatorios )
{
	int i,j,in;
	//int nsubmc=0;
	int ngr=0;
	//Energia
	double xold,yold;
	double xnew,ynew;

	//Nombre de los archivos
	char *outFileFConf=fileType( output, FINALCONF );
	char *outFileNMC=fileType( output, NMC );

	//Crear archivos de salida
	FILE *ff;
	FILE *fnmc;
	ff = fopen( outFileFConf, "w" );
	fnmc = fopen( outFileNMC, "w" );

	//Verificar que se haya podido crear archivo
	if( ff == NULL )
	{
		help( FILE_CREATE_ERROR, outFileFConf );
		free(outFileFConf);
		free(outFileNMC);
		return FILE_CREATE_ERROR;
	}

	//Verificar que se haya podido crear archivo NMC
	if( fnmc == NULL )
	{
		help( FILE_CREATE_ERROR, outFileNMC );
		free(outFileFConf);
		free(outFileNMC);
		return FILE_CREATE_ERROR;
	}

	//Reservamos memoria para el resultado
	double *pot=(double *)malloc(sizeof(double));
	*pot=0;

	//Declaramos apuntadores en la memoria del gpu
	float *devEnew;
	float *devEold;
	float *devX, *devY;
	int *devMov;
	int *devAccept;

	//Reservamos memoria en el gpu
	cudaMalloc( &devMov, sizeof(int) );
	cudaMalloc( &devEnew, sizeof(float) );
	cudaMalloc( &devEold, sizeof(float) );
	cudaMalloc( &devAccept, sizeof(int) );
	cudaMalloc( &devX, sizeof(float) * n );
	cudaMalloc( &devY, sizeof(float) * n );


	//Reseteamos memoria, ponemos arreglos en 0
	cudaMemset( devMov, 0, sizeof(int) );
	cudaMemset( devEnew, 0, sizeof(float) );
	cudaMemset( devEold, 0, sizeof(float) );
	cudaMemset( devAccept, 0, sizeof(int) );
	cudaMemset( devX, 0, sizeof(float) * n );
	cudaMemset( devY, 0, sizeof(float) * n );


	//Copiamos memoria del host a la memoria del GPU
	cudaMemcpy( devX, x, sizeof(float) * n, cudaMemcpyHostToDevice );
	cudaMemcpy( devY, y, sizeof(float) * n, cudaMemcpyHostToDevice );

	//Conseguir numero de bloques que usaremos
	int BLOCKS=( (n/800) + 1 );

	//DEBUG
	//Probando de forma secuencial
	nmc=1;
	int imc;
	for (imc=0;imc<nmc;imc++)
	{
		for (in=0;in<n;in++)
		{
			//Se consigue numero aleatorio
			i = rand()%n;
			particulas[in]=i;
			//Re set mov flag to 0
			//CUDA SET MOV TO 0
			cudaResetFlag<<<1,1>>>(devMov,0);

			//Take random Particle
			xold=x[i];
			yold=y[i];


			energiaParticulaCUDA<<< BLOCKS, THREADS >>>( devEold, devX, devY, xold, yold, i, n, lx, ly, lx2, ly2, Ra, Rr, Ra2, Rr2, epsilon_a, epsilon_r, sigma2 );


			//Create new particles
			xnew=traslation(xold, dpmax);
			ynew=traslation(yold, dpmax);


			condicionesPeriodicas(&xnew, &ynew, lx, lx2, ly, ly2);

			//Reset flag to default
			cudaResetFlag<<<1,1>>>(devMov,1);

			//Check in parallel if one particle traslape another
			traslapeParallel<<<BLOCKS, THREADS >>>(devMov, xnew, ynew, i, n, devX, devY, lx, lx2, ly, ly2, sigma2);

			float b=(float) rand()/RAND_MAX;
			aleatorios[in]=b;
			energiaParticulaCUDALastPart<<< BLOCKS, THREADS >>>( devEnew, devX, devY, xnew, ynew, i, n, lx, ly, lx2, ly2, Ra, Rr, Ra2, Rr2, epsilon_a, epsilon_r, sigma2, devMov, devEold, xold, yold, devAccept, b );

		}
		printf("IMC=%d\n", imc);
		ngr++;
	}
q
	//Recuperamos el resultado
	cudaMemcpy( x, devX, sizeof( float ) * n, cudaMemcpyDeviceToHost );
	cudaMemcpy( y, devY, sizeof( float ) * n, cudaMemcpyDeviceToHost );

	cudaFree(devMov);
	cudaFree(devEnew);
	cudaFree(devEold);
	cudaFree(devAccept);
	cudaFree(devX);
	cudaFree(devY);


/*	if ((imc % nsub) == 0)
	{
		nsubmc++;
		printf("nmc %d, nsubrutina %d, numero de rdf %d \n",imc,nsubmc,ngr);
		printf("aceptacion %f\n",(double)acept/(imc*n));
		energiaw(imc,Eold);
	}
*/
	for (j=0;j<n;j++)
	{
		fprintf(ff,"%f\t%f\n",x[j],y[j]);
	}
	int ngrtot=ngr+ngrold;
	fprintf(fnmc,"%d %d\n",nmctot,ngrtot);

	free(outFileFConf);
	free(outFileNMC);

        fclose(ff);
        fclose(fnmc);
	printf("Termina subrutina Monte Carlo \n");
	return 0;
}

//Main Program
int main ( int argc, char *argv[] )
{
	//Variables de entrada

	//nmc El número de pasos Monet Carlo
	int nmc;

	//nsub El número de subrutinas que aparece en pantalla
	//y cada vez que se guarda el valor de la energía en el archivo
	int nsub;

	//n Es el número de partículas
	int n;
	int nmcold;
	int ngrold;

	//rho Es la densidad
	float rho;
	float dpmax;
	char newprog;
	char orden;

	//Variables SIGMA
	//sigmna Es el diámetro de los árboles (partículas)
	float sigma,sigma2,sigma_m;
	//Potencial
	double epsilon_a, epsilon_r;
	float  Ra, Rr, Ra2, Rr2;
	float rc;

	//Variables para Area
	float area;
	float lx, ly;
	float lx2, ly2;
	float deltar;
	int ndivr;

	//Variable para el baby y monte c
	float dr;
	float *x=(float *) malloc( sizeof(float) * 50000 );
	float *y=(float *) malloc( sizeof(float) * 50000 );
	unsigned long *g=(unsigned long *) malloc( sizeof(unsigned long) * 5000 );

	//DEBUG
	//Probando de forma secuencial
	int CPUx[50000];
	int CPUy[50000];

	//Borra G en todas las funciones
	//unsigned long *g=(unsigned long *) malloc( sizeof(unsigned long) * 5000 );

	//Inicia calculo principal
	//Argv1 es el nombre del archivo de configuracion de entrada
	int openOK=readInput( argv[1], &nmc, &nsub, &n, &rho, &sigma, &Ra, &Rr, &dpmax, &newprog, &orden );

	//If the file wasnt able to open, then exit with failure code
	if( openOK < 0 )
	{
		printf("Couldnt open file :(\n");
		return EXIT_FAILURE;
	}

	//Calcular valor de Sigmas
	getSigmas( &sigma2, &sigma_m, sigma );

	//printf("Funcion: reducedUnits( sigma %f, &sigma2 %f, &sigma_m %f, &Ra %f, &Rr %f, &Ra2 %f, &Rr2 %f, &rc %f, &epsilon_a %f, &epsilon_r %f );\n",
	//	sigma, sigma2, sigma_m, Ra, Rr, Ra2, Rr2, rc, epsilon_a, epsilon_r);
	reducedUnits( sigma, &sigma2, &sigma_m, &Ra, &Rr, &Ra2, &Rr2, &rc, &epsilon_a, &epsilon_r );
	printf("Funcion despues de Reducir: reducedUnits( sigma %f, &sigma2 %f, &sigma_m %f, &Ra %f, &Rr %f, &Ra2 %f, &Rr2 %f, &rc %f, &epsilon_a %f, &epsilon_r %f );\n",
		sigma, sigma2, sigma_m, Ra, Rr, Ra2, Rr2, rc, epsilon_a, epsilon_r);

	//Calcular Areas
	areas( &area, n, rho, &lx, &ly, &lx2, &ly2, &deltar, &ndivr);

	//Crear Nombres de los Archivos de salida
	char *boxFileName=fileType( argv[2], BOX );
	char *metropoliFileName= fileType( argv[2], METROPOLIDIAM );
	char *mathematicaFileName= fileType( argv[2], MATHEMATICA );

	//Crear archivo caja
	int writeOK=writeBox( boxFileName, lx2, ly2);

	//Calcular Metropoli
	//metropolis( metropoliFileName );

	int status=0;

	//En caso de que no se alla podido crear archivo de salida, salir
	if(writeOK < 0)
	{
		printf("No se pudo crear archivo de salida :(\n");
		status=EXIT_FAILURE;
	}

	//Generacion de mejor semilla para los numeros random
	struct timeval timeValue;
	gettimeofday(&timeValue, NULL);
	srand(timeValue.tv_sec * timeValue.tv_usec);

	//Si se quiere crear configuracion nueva
	if( newprog == 'n' && status == 0 )
	{
		printf("Empieza configuracion nueva\n");
		initialConfig( argv[2], orden, x, y, lx, ly, lx2, ly2, n, &dr, sigma, sigma2, sigma_m );
		nmcold=0;
		ngrold=0;
	}
	else if( newprog == 'o' && status == 0 )
	{

		status+=readNmc( argv[1], &nmcold, &ngrold );
		status+=readConf( argv[1], x, y, n );
		status+=readRDF( argv[1], g, ndivr );
	}
	else
	{
		status=1;
	}

	//Si todo esta libre de errores
	if( status == 0 )
	{
		int nmctot=nmc+nmcold;
		int particulas[50000];
		float aleatorios[50000];
		memcpy(CPUx, x, sizeof(float) * 50000);
		memcpy(CPUy, y, sizeof(float) * 50000);
		//mc( argv[2], CPUx,  CPUy, g, nmc, n,  dpmax, lx, lx2, ly, ly2, sigma2, Ra, Rr, Ra2, Rr2, epsilon_a, epsilon_r, deltar, nmctot, ngrold );
		almostParallelMC( argv[2], x,  y, g, nmc, n,  dpmax, lx, lx2, ly, ly2, sigma2, Ra, Rr, Ra2, Rr2, epsilon_a, epsilon_r, deltar, nmctot, ngrold, particulas, aleatorios );

		for(i=0; i<50000; i++)
		{
			printf("Se eligio la particula %d Que tiene posicion (%f, %f)  y genero el numero aleatorio %f\n", particulas[i], x[particulas[i]], y[particulas[i]], aleatorios[i]);
		}

	}

	mathematica(mathematicaFileName, x, y, sigma_m, n );
	//Liberacion de memoria
	free(boxFileName);
	free(metropoliFileName);
	free(mathematicaFileName);
	free(x);
	free(y);
	free(g);
	return 0;
}



